import fetch from "node-fetch";

async function main() {
  const response = await fetch("http://localhost:3000/users");
  const users_data = await response.json();
  const userData = users_data['users']
  let users_todos_cnt = []
  let i = 1;
  const interval_of_1sec = setInterval(async () => {
    try {
      while (i <= userData.length) {
        const data = await fetch(`http://localhost:3000${userData[i - 1]['todos']}`)
        const dataJson = await data.json()
        const todos = (dataJson['todos'])
        let cnt = 0;
        for (let j = 0; j < todos.length; j++) {
          if (todos[j]['isCompleted']) {
            cnt++;
          }
        }
        users_todos_cnt.push({
          id: i,
          name: `User ${i}`,
          numTodosCompleted: cnt,
        })
        i++;
        if ((i - 1) % 5 == 0) {
          break;
        }
      } if (i > userData.length) {
        console.log(users_todos_cnt);
        clearInterval(interval_of_1sec)
      }
    } catch (error) {
      console.log(error);
    }
  }, 1000);
  interval_of_1sec
}
main();

// write your code here
